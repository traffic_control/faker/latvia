<?php

namespace Faker\Latvia;

use Faker\Extension\Extension;

class Internet extends \Faker\Provider\Internet implements Extension
{
    protected static $freeEmailDomain = ['mail.lv', 'apollo.lv', 'inbox.lv', 'gmail.com', 'yahoo.com', 'hotmail.com'];
    protected static $tld = ['com', 'net', 'org', 'lv',];
}
