<?php

namespace Faker\Latvia;

use Faker\Extension\Extension;

class PhoneNumber extends \Faker\Provider\PhoneNumber implements Extension
{
    /**
     * {@link} https://en.wikipedia.org/wiki/Telephone_numbers_in_Latvia
     */
    protected static $formats = [
        '60# #####',
        '61# #####',
        '62# #####',
        '630 #####',
        '631 #####',
        '632 #####',
        '633 #####',
        '634 #####',
        '635 #####',
        '636 #####',
        '637 #####',
        '638 #####',
        '639 #####',
        '640 #####',
        '641 #####',
        '642 #####',
        '643 #####',
        '644 #####',
        '645 #####',
        '646 #####',
        '647 #####',
        '648 #####',
        '649 #####',
        '650 #####',
        '651 #####',
        '652 #####',
        '653 #####',
        '654 #####',
        '655 #####',
        '656 #####',
        '657 #####',
        '658 #####',
        '659 #####',
        '66# #####',
        '67# #####',
        '680 #####',
        '682 #####',
        '683 #####',
        '684 #####',
        '685 #####',
        '686 #####',
        '687 #####',
        '688 #####',
        '689 #####',
        '69# #####',
        '789 #####',
        '2## ## ###',
        '+371 6#######',
    ];

    protected static $mobileFormats = [
        '+371 2#######',
        '2#######',
    ];

    /**
     * @example '212 35 246'
     *
     * @return string
     */
    public function mobilePhoneNumber()
    {
        return static::numerify($this->generator->parse(static::randomElement(static::$mobileFormats)));
    }
}
