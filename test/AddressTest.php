<?php

namespace Faker\Test\Latvia;

use Faker\Generator;
use Faker\Latvia\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    const REPLACE_NON_NUMERIC_REGEX = '/[^0-9]/';
    /**
     * @var Generator
     */
    private $_faker;

    protected function setUp(): void
    {
        $faker = new Generator();
        $faker->addProvider(new Address($faker));
        $this->_faker = $faker;
    }

    /**
     * Test the validity of postcode
     */
    public function testPostalCode()
    {
        $postcode = $this->_faker->postcode();

        $this->assertNotEmpty($postcode);

        $postCodeNubmers = preg_replace(self::REPLACE_NON_NUMERIC_REGEX, '', $postcode);

        $this->assertTrue(is_numeric($postCodeNubmers));
        $this->assertEquals(4, strlen($postCodeNubmers));
    }
}
