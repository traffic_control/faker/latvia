Under development
-----------------------------------------------

1.1.0 2023-05-29
-----------------------------------------------
- Feature: Add mobilephone number generator (Kalmer)

1.0.0 2023-05-24
-----------------------------------------------
- Feature: Initial implementation (Kalmer)
